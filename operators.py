# Arithmetic operators
a = 10
b = 3

addition = a + b
subtraction = a - b
multiplication = a * b
division = a / b
floor_division = a // b
modulus = a % b
exponentiation = a ** b

print("Arithmetic Operators:")
print("Addition:", addition)
print("Subtraction:", subtraction)
print("Multiplication:", multiplication)
print("Division:", division)
print("Floor Division:", floor_division)
print("Modulus:", modulus)
print("Exponentiation:", exponentiation)
print()

# Comparison operators
x = 5
y = 7

print("Comparison Operators:")
print("x < y:", x < y)
print("x > y:", x > y)
print("x <= y:", x <= y)
print("x >= y:", x >= y)
print("x == y:", x == y)
print("x != y:", x != y)
print()

# Assignment operators
z = 10

z += 5
print("Assignment Operators:")
print("z += 5 (Equivalent to z = z + 5):", z)

z -= 3
print("z -= 3 (Equivalent to z = z - 3):", z)

z *= 2
print("z *= 2 (Equivalent to z = z * 2):", z)

z /= 4
print("z /= 4 (Equivalent to z = z / 4):", z)

z %= 3
print("z %= 3 (Equivalent to z = z % 3):", z)

z **= 2
print("z **= 2 (Equivalent to z = z ** 2):", z)

z //= 2
print("z //= 2 (Equivalent to z = z // 2):", z)
print()

# Logical operators
p = True
q = False

print("Logical Operators:")
print("p and q:", p and q)
print("p or q:", p or q)
print("not p:", not p)
print()

# Bitwise operators
num1 = 10
num2 = 6

print("Bitwise Operators:")
print("num1 & num2:", num1 & num2)
print("num1 | num2:", num1 | num2)
print("num1 ^ num2:", num1 ^ num2)
print("num1 << 2:", num1 << 2)
print("num1 >> 2:", num1 >> 2)
print("~num1:", ~num1)
print()

# Membership operators
list1 = [1, 2, 3, 4, 5]

print("Membership Operators:")
print("3 in list1:", 3 in list1)
print("6 not in list1:", 6 not in list1)
print()

# Identity operators
x = 10
y = 10
z = x

print("Identity Operators:")
print("x is y:", x is y)
print("x is z:", x is z)
print("x is not z:", x is not z)
